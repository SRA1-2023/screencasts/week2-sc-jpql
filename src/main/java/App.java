import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {
	private static Logger log = LoggerFactory.getLogger( App.class );
	
	public static void main(String argv[]) {
		System.out.println("Running App ...");
		
		log.debug("Create persistence manager");
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("myApp");
		EntityManager em = emf.createEntityManager();
		
		log.debug("Search for French Airlines");
		
		log.debug("Search for French Airlines going to Miami");

		log.debug("Search Number of airlines from CDG");
		
		log.debug("Search Best airlines with number of dest airports to USA from CDG, ordered by 'coverage'");
		
		log.debug("Close Entity Manager");
		em.close();
		emf.close();

	}
}